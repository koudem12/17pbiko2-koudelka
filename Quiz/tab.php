<?php 

?>
<!DOCTYPE html>
<html lang="en-US">
<head>
  <meta charset="utf-8">
  <meta name="author" content="Koudelka Matěj">
 <title>17PBIKO2 Koudelka Quiz And Materials </title>
  <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
</head>

<body>

<div class="wrapper">

	
	<?php include 'nabidka.php' ?> 
<!-- Text Stránky -->
<table id="table">
  <tr>                        
    <th><b>Popis</b></th>
    <th><b>Odkaz</b></th>
  </tr>
  
<?php
?>
  
  <tr>
    <td>V této lekci se naučíte něco o cukrovce prvního typu</td>
    <td> <a href = https://predmety.fbmi.cvut.cz/sites/default/files/predmet/1337/cviceni/17PBIKO2_20160207_172145_91aff4c72d82e435a46efba3e09a5524.ppt </a> Diabetes mellitus I </td>
  </tr>
 <tr>
    <td>V této lekci se naučíte něco o cukrovce druhého typu</td>
     <td> <a href = https://predmety.fbmi.cvut.cz/sites/default/files/predmet/1337/cviceni/17PBIKO2_20200402_212631_cda0562cbc9d056f42cee9672650d0d5.pdf </a> Diabetes mellitus II </td>
  </tr>
   <tr>
    <td>V této lekci se naučíte něco o o krvi, její normalitě, způsobech odběru, nemocech a defektech</td>
     <td> <a href = https://predmety.fbmi.cvut.cz/sites/default/files/predmet/1337/cviceni/17PBIKO2_20160207_172145_9357712701e308bbc5a2932502b4894a.ppt </a> Hepatologie </td>
  </tr>
   <tr>
    <td>V této lekci se naučíte něco o tom jak anestetika působí na organismus, jeho vedlejší účinky a jak tělo zpracovává cizí látky v těle</td>
     <td> <a href = https://predmety.fbmi.cvut.cz/sites/default/files/predmet/1337/cviceni/17PBIKO2_20200401_162255_c19b88619c3c2cc7936a49b1da46bdb7.pdf </a> Jak působí anestetika </td>
  </tr>
   <tr>
    <td>V této lekci se naučíte něco o průběhu epidemie, jak se proti epidemii bránit, státních nařízeních a jak takovýmto epidemiím předcházet</td>
     <td> <a href = https://predmety.fbmi.cvut.cz/sites/default/files/predmet/1337/cviceni/17PBIKO2_20200401_162255_4fc55ded4d24c8dbc55cd6589a9d6073.pdf </a> Epidemický proces </td>
  </tr>

  <?php

  ?>
<!-- Kontaktní informace -->
	<?php include 'footer.php' ?> 

</div>

</body>
</html>