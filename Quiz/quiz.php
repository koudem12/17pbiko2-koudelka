<?php 

?>
<!DOCTYPE html>
<html lang="en-US">
<head>
  <meta charset="utf-8">
  <meta name="author" content="Koudelka Matěj">
 <title>17PBIKO2 Koudelka Quiz And Materials </title>
  <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="style.css">
  <link rel="stylesheet" type="text/css" href="quiz.css">
</head>
</head>

<body>

<div class="wrapper">

	
	<?php include 'nabidka.php' ?> 
<!-- Text Stránky -->

<div id="main"><!-- open main div -->
<div id="header"><!-- open header div -->
<h1>Quiz Koudelka</h1>
</div><!-- close header div -->
<form id="form1">

<h2>Jak vzniká Diabetes 1. typu?</h2>
<label for="var_string"><input type="radio" name="variable" value="0" id="var_string" />Porucha pankreatu. Vzniká v dospělosti a nelze léčit.</label>
<label for="var_join"><input type="radio" name="variable" value="0" id="var_join" />Porucha icteru. Vzniká v dětství a nelze léčit.</label>
<label for="var_info"><input type="radio" name="variable" value="20" id="var_info" />Porucha pankreatu. Vzniká v dětství a nelze léčit.</label>
<label for="var_condition"><input type="radio" name="variable" value="0" id="var_condition"/>Porucha pankreatu. Vzniká v dětství a lze léčit.</label>

<h2>Jaké jsou formy epidemického procesu ve správném pořadí?</h2>
<label for="sub_string"><input type="radio" name="sub" value="20" id="sub_string"/>1. Sporadický výskyt, 2. epidemický výskyt, 3. pandemický výskyt, 4. endemický výskyt</label>
<label for="sub_join"><input type="radio" name="sub" value="0" id="sub_join"/>1. Epidemický výskyt, 2. sporadický výskyt, 3. pandemický výskyt, 4. endemický výskyt</label>
<label for="sub_info"><input type="radio" name="sub" value="0" id="sub_info" />1. sporadický výskyt, 2. pandemický výskyt, 3. endemický výskyt</label>
<label for="sub_condition"><input type="radio" name="sub" value="0" id="sub_condition" />1. sporadický výskyt, 2. pandemický výskyt, 3. endemický výskyt, 4. sporadický výskyt</label>

<h2>Jak se přenáší Hepatitida B</h2>
<label for="cat_string"><input type="radio" name="con" value="0" id="cat_string" />Krví, spermatem, z matky na syna</label>
<label for="cat_join"><input type="radio" name="con" value="20" id="cat_join" />Krví, slinami, spermatem, z matky na syna</label>
<label for="cat_info"><input type="radio" name="con" value="0" id="cat_info" />Krví, slinami, z matky na syna</label>
<label for="cat_condition"><input type="radio" name="con" value="0" id="cat_condition" />Krví</label>

<h2>Celková vitální kapacita normálního muže je?</h2>
<label for="if_string"><input type="radio" name="ifstate" value="0" id="if_string" />5l</label>
<label for="if_join"><input type="radio" name="ifstate" value="0" id="if_join" />5,5l</label>
<label for="if_info"><input type="radio" name="ifstate" value="0" id="if_info" />4,2l</label>
<label for="if_condition"><input type="radio" name="ifstate"  value="20" id="if_condition" />6l</label>

<h2>Jak se léčí Diabetes?</h2>
<label for="pet_string"><input type="radio" name="pet" value="0" id="pet_string" />Cvičení a farmatologie</label>
<label for="pet_join"><input type="radio" name="pet" value="0" id="pet_join" />Dieta a farmakologie</label>
<label for="pet_info"><input type="radio" name="pet" value="0" id="pet_info" />Farmatologie</label>
<label for="pet_condition"><input type="radio" name="pet"  value="20" id="pet_condition" />Dieta, cvičení, Farmatologie</label>
<button type="submit" value="Submit">Vyhodnoť</button>

</form>


<p><font size="40">Vaše známka je: <b><span id="grade" >__</span></b></font></p>
<p id="grade2"></p>

</div><!-- close main div -->

<script>
document.getElementById("form1").onsubmit=function() {
       variable = parseInt(document.querySelector('input[name = "variable"]:checked').value);
	   sub = parseInt(document.querySelector('input[name = "sub"]:checked').value);
	   con = parseInt(document.querySelector('input[name = "con"]:checked').value);
	   ifstate = parseInt(document.querySelector('input[name = "ifstate"]:checked').value);
	   pet = parseInt(document.querySelector('input[name = "pet"]:checked').value);

	   result = variable + sub + con + ifstate + pet;
		if (result == 0){result = "F s 0 body"};
		if (result == 20) {result = "E s 20 body"};
		if (result == 40) {result = "D s 40 body"};
		if (result == 60) {result = "C s 60 body"};
		if (result == 80) {result =  "B s 80 body"};
		if (result == 100) {result = "A se 100 body"};


	   
	document.getElementById("grade").innerHTML = result;
	   


return false; // required to not refresh the page; just leave this here
} //this ends the submit function

</script>







<!-- Kontaktní informace -->
	<?php include 'footer.php' ?> 

</div>

</body>
</html>



