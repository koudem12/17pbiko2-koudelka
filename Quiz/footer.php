<div class="container" id="info">
	    <div class="row">
	    	<div class="col-xs-9 col-sm-9 col-md-9"><span class="glyphicon glyphicon-user"></span> Matěj Koudelka, FBMI, BMI</div>
	    	<div class="col-xs-3 col-sm-3 col-md-3"><span class="glyphicon glyphicon-envelope"></span><a href="mailto:#" target="_blank"> koudem12@cvut.fbmi.cz</a></div>	
	    </div>
	    <div class="row">
	    	<div class="col-xs-9 col-sm-9 col-md-9"><span class="glyphicon glyphicon-time"></span> Duben 2020</div>
	    	<div class="col-xs-3 col-sm-3 col-md-3"><a href="https://predmety.fbmi.cvut.cz/cs/17PBIKO2" target="_blank"><span class="glyphicon glyphicon-globe"></span> Stránky Předmětu CVUT FBMI</a></div>
	    </div>
    </div>